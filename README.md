## Introduction

This project is a template using Vite with Redux and some other popular libraries in React.

## Prerequisites

To install and run the project, you need to have the following prerequisites:

- [Node.js](https://nodejs.org/): Version >= 14.17.0
- [npm](https://www.npmjs.com/): Version >= 6.14.13 : Version >= 1.22.11
- [TypeScript](https://www.typescriptlang.org/): Version >= 4.5.2
- [Vite](https://vitejs.dev/): Version >= 4.0.0

## Installation

1. Clone the repository:

```https://gitlab.com/final_project_bap/finalproject_fe.git```
```bash
cd gallery
```
## Install dependencies:
```bash
npm install
```
## Run the application:
```bash
npm run dev
```

## Main Libraries
**@reduxjs/toolkit:**   ```Version ^1.9.7  ```
**react:**   ```Version ^18.2.0  ```
**vite:**  ```Version ^4.0.0  ```
Other libraries are also listed in the package.json file.