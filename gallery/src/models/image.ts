export interface    uploadImage{
    image: File;
}

export interface images{
    _id:string
    image_name: string
    imageURL: string
    status: string
    public_id: string
    favorite_image: string
}