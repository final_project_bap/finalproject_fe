export interface User{
    _id: string
    name: string,
    email: string,
}

export interface CreateUser {
    email: string;
    name: string;
    password: string;
}

