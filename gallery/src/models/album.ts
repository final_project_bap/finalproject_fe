export interface album {
    _id: string,
    album_name: string,
}
export interface shareAlbum{
    album_id: string
    listEmail: string[]
}
interface Folder {
    _id: string
    album_name: string,
}