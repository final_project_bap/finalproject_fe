import {Route, Routes, useRoutes} from "react-router-dom"
import Layout from "./components/layout/Layout";
import {Login} from "./features/auth/pages/Login";
import {SignUp} from "./features/auth/pages/SignUp";
import {ForgotPassword} from "./features/auth/pages/ForgotPassword";
import React from "react";
import {useAppSelector} from "./app/hooks";
import {selectIsLoggedIn} from "./features/auth/authSlice";
import {RecoverPassword} from "./features/auth/pages/RecoverPassword";

import ShowImages from "./features/image/pages/showImages";
import {FolderImage} from "./features/home/pages/FolderImage";
import {ImageOfFolder} from "./features/home/pages/ImageOfFolder";
import {selectListImageResponse} from "./features/image/imageSlice";
import SelectImage from "./components/SelectImage/pages/SelectImage";
import {selectShowSlide} from "./components/SelectImage/selectImageSlice";
import LoaderBanner from "./components/loader/loader";
import {selectLoading} from "./components/loader/loadingSlice";
import {FolderShare} from "./features/share/pages/FolderShare";
import ImageInShareFolder from "./features/share/pages/imageInShareFolder";
import ShareImage from "./features/share/pages/ShareImage";

function App() {

    let isAuthenticated = useAppSelector(selectIsLoggedIn)
    const isShowSlide = useAppSelector(selectShowSlide)
    const loading = useAppSelector(selectLoading)
    return (
        <>
            {loading && <LoaderBanner/>}
            {isShowSlide && <SelectImage/>}
            <Routes>
                {
                    isAuthenticated
                        ? <>
                            <Route path="/" element={<Layout/>}>
                                <Route index element={<ShowImages/>}/>
                                <Route path={"/favorite"} element={<ShowImages/>}/>
                                <Route path={"/album"} element={<FolderImage/>}/>
                                <Route path={"/:folder/:id"} element={<ImageOfFolder/>}/>
                                <Route path={"/share/:folder/:id"} element={<ImageInShareFolder/>}/>
                                <Route path={"/share"} element={<FolderShare/>}/>
                                <Route path={"*"} element={<ShowImages/>}/>
                            </Route>
                        </>
                        : <>
                            <Route path={"/images/:id"} element={<ShareImage/>}/>

                            <Route path="/login" element={<Login/>}/>
                            <Route path="/register" element={<SignUp/>}/>
                            <Route path="/forgot-password" element={<ForgotPassword/>}/>
                            <Route path="/resetPassword/:token" element={<RecoverPassword/>}/>
                            <Route path="*" element={<Login/>}/>
                        </>
                }
            </Routes>
        </>
    )
}

export default App
