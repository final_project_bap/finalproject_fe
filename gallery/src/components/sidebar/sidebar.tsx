import {faImage, faPhotoFilm, faShare, faStar, faTrash} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import './sidebar.css'
import {useAppDispatch} from "../../app/hooks";
import {imageActions} from "../../features/image/imageSlice";
import { Link, useLocation, useParams } from "react-router-dom";
const Sidebar = () => {
    const location=useLocation()
    const {folder,id}:any=useParams()
    const encodedFolder = encodeURIComponent(folder);
    const dispatch = useAppDispatch()
    const handleOnclickImage= () =>{
        dispatch(imageActions.gettingImages())
    }
    const handleOnclickFavouriteImage= () =>{
        dispatch(imageActions.gettingFavouriteImages())
    }
  return(
      <>
          <div className="side-bar">
              <Link to={"/"} onClick={() => {handleOnclickImage()}} className={`side-bar-item ${location.pathname=="/"?"side-bar-item-background":""}`}>
                  <FontAwesomeIcon icon={faImage} />
                  <p>Photos</p>
              </Link>
              <Link to={"/favorite"} onClick={() => {handleOnclickFavouriteImage()}} className={`side-bar-item ${location.pathname=="/favorite"?"side-bar-item-background":""}`}>
                  <FontAwesomeIcon icon={faStar} />
                  <p>Favorites</p>
              </Link>
              <Link to={"/share"} className={`side-bar-item ${location.pathname=="/share" || location.pathname==`/share/${encodedFolder}/${id}` ? "side-bar-item-background":""}`}>
                  <FontAwesomeIcon icon={faShare} />
                  <p>Share with me</p>
              </Link>
              <Link to={"/album"} className={`side-bar-item ${location.pathname=="/album" || location.pathname==`/${encodedFolder}/${id}` ? "side-bar-item-background":""}`}>
                  <FontAwesomeIcon icon={faPhotoFilm} />
                  <p>Albums</p>
              </Link>

          </div>
      </>
  )
}


export default Sidebar