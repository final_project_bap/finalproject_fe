import './header.css'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowUpFromBracket, faMagnifyingGlass } from "@fortawesome/free-solid-svg-icons";
import React, { useRef, useState } from "react";
import { useAppDispatch } from "../../app/hooks";
import { authActions } from "../../features/auth/authSlice";
import { getRefreshToken} from "../../utils";
import {imageActions} from "../../features/image/imageSlice";
import {toast} from "react-toastify";


const Header = () => {
    const fileInputRef=useRef<HTMLInputElement>(null)
    const [showDropMenu, setShowDropMenu] = useState(false)
    const dispatch = useAppDispatch()
    const handleShowDropMenu = () => {
        setShowDropMenu(!showDropMenu)
    }
    const isValidFileType = (file: File) => {
        const allowedTypes = ['image/jpeg', 'image/png'];
        return allowedTypes.includes(file.type);
    };

    const handleLogout =  () => {
        const refreshToken: any =  getRefreshToken()
            dispatch(authActions.logout(refreshToken))
    }
    const handleUploadImage=(e:React.MouseEvent)=>{
        fileInputRef.current?.click()
    }
    const handleImageChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const files = event.target.files;

        if (files && files.length > 0) {

            const maxSize = 10 * 1024 * 1024;
            const validFiles = Array.from(files).every((file) => file.size <= maxSize && isValidFileType(file));

            if (validFiles) {

                dispatch(imageActions.uploadImage(files))
            } else {
                // Some files exceed 10MB
                toast.warning('Each selected image should be below 10MB.');
            }
        }}
    return (
        <>
            <div className="header">
                <div className="logo">
                    <p>My Photos</p>
                </div>

                <div className="profile-bar">
                    <div className="upload" onClick={handleUploadImage}>
                        <div className="upload-button">
                            <FontAwesomeIcon icon={faArrowUpFromBracket} />
                            <form action="" method="post">
                                <input
                                    type="file"
                                    name=""
                                    id=""
                                    style={{display: 'none'}}
                                    ref={fileInputRef}
                                    onChange={handleImageChange}
                                    multiple  // Allow multiple file selection
                                />
                            </form>
                            <p>Upload</p>
                        </div>
                    </div>
                    <div onClick={() => {
                        handleShowDropMenu()
                    }} className="profile">
                        <img
                            src="https://th.bing.com/th/id/R.67115e53caf513401314a51fd89f0066?rik=MT2f6Atefcbuvg&pid=ImgRaw&r=0"
                            alt="" />

                        {
                            showDropMenu
                            &&
                            <div className="dr-menu">
                                <p>View profile</p>
                                <p onClick={() => { handleLogout() }}>Logout</p>
                            </div>
                        }
                    </div>
                </div>
            </div>
        </>
    )
}
export default Header