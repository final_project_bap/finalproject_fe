import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {RootState} from "../../app/store";
import {images} from "../../models";

export interface SelectImageState {
    loading: boolean
    showSlide: boolean,
    imagesSelect: images[],
    currentSlide: number

}

const initialState: SelectImageState = {
    loading: false,
    showSlide: false,
    imagesSelect: [],
    currentSlide: 0
}

export  interface  selectImage{
    index: number,
    images: images[]
}

const selectImageSlice = createSlice({
    name: "selectImage",
    initialState,
    reducers: {
        selectingImage(state,  action: PayloadAction<selectImage>) {
            state.loading = true
        },

        selectedImage(state, action: PayloadAction<selectImage>) {
            state.loading = false
            state.showSlide = true
            state.imagesSelect = action.payload.images
            state.currentSlide = action.payload.index
        },

        closeSelectedImage(state){
          state.showSlide = false
        },
        deleteImage(state, action: PayloadAction<string>){
            state.showSlide = false
        },
        deleteImageSuccess(state){
            state.showSlide = false
        },
        deleteImageFailed(state){
            state.showSlide = false
        },

    }

})


export const selectImageActions = selectImageSlice.actions

export const selectCurrentSlide = (state: RootState) => state.selectImage.currentSlide
export const selectImagesSelect = (state: RootState) => state.selectImage.imagesSelect
export const selectShowSlide = (state: RootState) => state.selectImage.showSlide


const selectImageReducer = selectImageSlice.reducer

export default selectImageReducer