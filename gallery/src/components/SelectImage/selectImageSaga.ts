import {all, takeEvery, put, take, takeLatest, call} from "redux-saga/effects";
import {selectImage, selectImageActions} from "./selectImageSlice";
import {PayloadAction} from "@reduxjs/toolkit";
import {imageActions} from "../../features/image/imageSlice";
import imageApi from "../../api/imageApi";
import {loadingActions} from "../loader/loadingSlice";
import {toast} from "react-toastify";



function* handleSelectImage(){
    const action: PayloadAction<selectImage>  = yield take(selectImageActions.selectingImage.type)
    yield put(selectImageActions.selectedImage(action.payload))
}

function* watchSelectImages(){
    yield takeEvery(selectImageActions.selectingImage.type, handleSelectImage);
}

function* handleCloseSelectImage(){
    yield put(selectImageActions.closeSelectedImage)
}

function* watchCloseSelectImage(){
    takeLatest(selectImageActions.closeSelectedImage.type, handleCloseSelectImage)
}

function* watchDeleteImage() {
    yield takeEvery(selectImageActions.deleteImage.type, handleDeleteImage);
}
function* handleDeleteImage(action: PayloadAction<string>) {
    try {
        yield put(loadingActions.setLoading(true))
        const data:string = yield call(imageApi.deleteImage, action.payload);
        yield put(imageActions.gettingImages())
        yield put(loadingActions.setLoading(false))
        toast.success("da xoa")

    } catch (error) {
        toast.warning("delete failed")

        yield put(imageActions.uploadImageFailed())
    }
}




function* watchAddFavouriteImage() {
    yield takeEvery(imageActions.addFavouriteImage.type, handleAddFavouriteImage);
}
function* handleAddFavouriteImage(action: PayloadAction<string>) {
    try {
        yield put(loadingActions.setLoading(true))
        const data:string = yield call(imageApi.addFavouriteImage, action.payload);
        yield put(loadingActions.setLoading(false))
        yield put(imageActions.gettingImages())
        toast.success("add success")

    } catch (error) {
        toast.warning("add failed")
        yield put(imageActions.uploadImageFailed())
    }
}
export  function * selectImageSaga(){
    yield all([
        watchSelectImages(),
        watchCloseSelectImage(),
        watchDeleteImage(),
        watchAddFavouriteImage()
    ]);
}