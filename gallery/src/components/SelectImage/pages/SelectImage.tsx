import React, {useEffect, useState} from 'react';
import './SelectImage.css'
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';


import {images} from "../../../models";
import {
    faArrowLeft,
    faChevronLeft,
    faChevronRight,
    faCircleInfo, faDownload,
    faEllipsisVertical,
    faShare,
    faStar,
    faTrash
} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {useAppDispatch, useAppSelector} from "../../../app/hooks";
import {selectCurrentSlide, selectImageActions, selectImagesSelect} from "../selectImageSlice";
import {imageActions} from "../../../features/image/imageSlice";
import imageApi from "../../../api/imageApi";
import {toast} from "react-toastify";




const SelectImage = () => {

    const images = useAppSelector(selectImagesSelect)

    const [currentSlide, setCurrentSlide] = useState(useAppSelector(selectCurrentSlide) || 0);
    const dispatch = useAppDispatch();
    const nextSlide = () => {
        const nextIndex = (currentSlide + 1);
        setCurrentSlide(nextIndex);
    };

    const prevSlide = () => {
        const prevIndex = (currentSlide - 1);
        setCurrentSlide(prevIndex);
    };

    const handleCloseImage = () => {
        dispatch(selectImageActions.closeSelectedImage())
    }
    const handleDeleteImage = (id: string) => {

        dispatch(selectImageActions.deleteImage(id))
    }
    const handleDownloadImage = async (id: string) => {
        try {
           ` const response = await imageApi.download(id);
            // Check if the response contains image data
            const contentType = response.headers && response.headers['content-type'] ? response.headers['content-type'] : 'image/jpeg';

            const blob = new Blob([response.data], { type: contentType });
            const url = window.URL.createObjectURL(blob);
            const a = document.createElement('a');
            a.href = url;
            a.download = 'image.jpg'; // You can set the desired file name here
            document.body.appendChild(a);
            a.click();
            window.URL.revokeObjectURL(url);`
        } catch (error) {
            console.error('Error downloading image:', error);
        }
    }
    const handleAddFavouriteImage = (id: string) => {
        dispatch(imageActions.addFavouriteImage(id))


    }
    const handleRemoveFavouriteImage = (id: string) => {
        imageApi.removeFavouriteImage(id).then((res) => {
            if (currentSlide >0){
                setCurrentSlide(currentSlide-1)
            }
            dispatch(imageActions.gettingFavouriteImages())
            dispatch(selectImageActions.closeSelectedImage())

            toast.success(`remove success `)
        }).catch((error) => {
            console.log(error);
        })
    }


    return (
        <>
            {
                images.length != 0 && <>
                    <div className="slide-container">
                        <div className="list-btn">
                            <div className="btn-back" onClick={() => {
                                handleCloseImage()
                            }}>
                                <FontAwesomeIcon className={"btn-selectImg"} icon={faArrowLeft}/>
                            </div>

                            <div className="list-btn-selectImg">
                                <FontAwesomeIcon className={"btn-selectImg"} icon={faShare}/>
                                <FontAwesomeIcon className={"btn-selectImg"} icon={faCircleInfo}/>

                                {
                                    images[currentSlide].favorite_image ?
                                        <FontAwesomeIcon  style={{color: "red"}}  onClick={() => {
                                            handleRemoveFavouriteImage(images[currentSlide]._id)
                                        }} className={"btn-selectImg"} icon={faStar}/>
                                        :
                                        <FontAwesomeIcon onClick={() => {
                                            handleAddFavouriteImage(images[currentSlide]._id)
                                        }} className={"btn-selectImg"} icon={faStar}/>

                                }

                                <FontAwesomeIcon onClick={() => {
                                    handleDownloadImage(images[currentSlide]._id)
                                }} className={"btn-selectImg"} icon={faDownload}/>

                                <FontAwesomeIcon onClick={() => {
                                    console.log(images[currentSlide])
                                    handleDeleteImage(images[currentSlide].public_id)
                                }} className={"btn-selectImg"} icon={faTrash}/>
                                <FontAwesomeIcon className={"btn-selectImg"} icon={faEllipsisVertical}/>
                            </div>
                        </div>

                        {currentSlide != 0 && <div onClick={() => {
                            prevSlide()
                        }} className="btn-left">
                            <FontAwesomeIcon className={"slide-btn-icon"} icon={faChevronLeft}/>
                        </div>}

                        <div className="slide-container">
                            {
                                <img className={"slide-image"} src={images[currentSlide].imageURL} alt="img "/>
                            }
                        </div>

                        {
                            currentSlide + 1 != images.length &&
                            <div onClick={() => {
                                nextSlide()
                            }} className="btn-right">
                                <FontAwesomeIcon className={"slide-btn-icon"} icon={faChevronRight}/>
                            </div>
                        }
                    </div>
                </>
            }
        </>
    )

}

export default SelectImage