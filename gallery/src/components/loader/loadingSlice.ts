import { createSlice } from '@reduxjs/toolkit';
import {RootState} from "../../app/store";
export interface loadingState {
    isLoading: boolean
}
const initialState:loadingState = {
    isLoading: false
}

export const loadingSlice = createSlice({
    name: 'loading',
    initialState,
    reducers: {
        setLoading: (state, action) => action.payload,
    },
});

const loadingReducer = loadingSlice.reducer

export const loadingActions = loadingSlice.actions
export  const selectLoading = (state : RootState) => state.loading.isLoading
export default loadingReducer;