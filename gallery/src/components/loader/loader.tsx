import {PacmanLoader} from "react-spinners";
import './loader.css'
const LoaderBanner = () =>{
    return(
        <>
            <div className="loader-banner">
                <PacmanLoader className={"loader-icon"} color="#36d7b7" />
            </div>
        </>

    )
}

export default LoaderBanner