import React, {ReactNode, useEffect, useState} from "react";
import {useAppDispatch, useAppSelector} from "../../../app/hooks";
import {Modal, Select} from "antd";
import {faFolder, faPlus, faShare, faTrash} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {ContextMenu, ContextMenuTrigger, MenuItem} from "react-contextmenu";
import {Link} from "react-router-dom";
import LoaderBanner from "../../../components/loader/loader";

import {album} from "../../../models";
import {selectAlbumShare, selectAlbumShareLoading, shareAlbumActions} from "../shareSlice";
import emptyFolder from "../../home/pages/544262.png";

export const FolderShare = () => {
    const loading = useAppSelector(selectAlbumShareLoading)
    const dispatch = useAppDispatch()

    useEffect(() => {
        dispatch(shareAlbumActions.gettingAlbumShare())
    }, [dispatch])

    const listAlbum = useAppSelector(selectAlbumShare)

    return (

        <>
            {loading && <LoaderBanner/>}

            <div className="folder-container">
                <div className="row">
                    {listAlbum.length != 0 ?listAlbum.map((item:album, index:number) => (
                        <>
                            <ContextMenuTrigger id={`image-context-menu-${index}`} key={index}>
                                <Link to={`/share/${item.album_name}/${item._id}`} key={index}>
                                    <div className="col-md-6 folder-container">
                                        <FontAwesomeIcon className={"icon-folder"} icon={faFolder}/>
                                        <span>{item.album_name}</span>
                                    </div>
                                </Link>

                            </ContextMenuTrigger>
                        </>
                    )):
                        <>
                            <div className="empty-folder">
                                <img src={emptyFolder} alt=""/>
                                <p>no folder</p>
                            </div>
                        </>
                    }
                </div>
            </div>

        </>
    )
}
