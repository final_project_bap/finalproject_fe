import LoaderBanner from "../../../components/loader/loader";
import emptyImage from "../../image/pages/Gallery-PNG-Download-Image.png";
import {useAppDispatch, useAppSelector} from "../../../app/hooks";
import React, {useEffect, useState} from "react";
import {imageActions, selectListImageResponse, selectLoading} from "../../image/imageSlice";
import {selectImageActions} from "../../../components/SelectImage/selectImageSlice";
import {Modal} from "antd";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faArrowLeft, faArrowUpFromBracket} from "@fortawesome/free-solid-svg-icons";
import imageApi from "../../../api/imageApi";
import {homeActions, selectGallery} from "../../home/homeSlice";
import {useNavigate, useParams} from "react-router-dom";

const ImageInShareFolder = () => {
    const { id }: any = useParams()
    const navigate = useNavigate()
    const [newFolder, setNewFolder] = useState(false)
    const [exitImage, setExitImage] = useState<number[]>([])
    const [idImage, setIdImage] = useState<any[]>([])
    const dispatch = useAppDispatch()
    const loading = useAppSelector(selectLoading)
    const images = useAppSelector(selectGallery)
    const imageAdd = useAppSelector(selectListImageResponse)

    const { folder } = useParams()
    useEffect(() => {
        dispatch(imageActions.gettingImages())
        dispatch(homeActions.gettingGallery(id))
    }, [dispatch])
    const handleShowSlide = (index: number) => {
        dispatch(selectImageActions.selectingImage({index:index, images: images}))
    }
    const [border, setBorder] = useState<string[]>(
        Array(images.length).fill("")
    )



    const handleChooseImage = (index: number, id: string) => () => {
        const updateBorder = [...border]
        updateBorder[index] = border[index - 1] == "choose-image" ? "" : "choose-image"
        setBorder(updateBorder)
        if (exitImage.includes(index)) {
            setExitImage(exitImage.filter((indexImage) => indexImage != index))
        } else {
            setExitImage([...exitImage, index])
        }
        if (idImage.includes(id)) {
            setIdImage(idImage.filter((idImage) => idImage != id))
        } else {
            setIdImage([...idImage, id])
        }
    }

    const handleUploadImageToFolder = () => {

        imageApi.uploadImageToFolderApi(id, idImage)
            .then((res: any) => {
                dispatch(homeActions.gettingGallery(id))
                setNewFolder(false)
            })
            .catch((error) => {
                console.log(error);
            })
    }


    return (
        <>
            {loading && <LoaderBanner/>}
            <div className='turn-back-page'>
                <FontAwesomeIcon icon={faArrowLeft} onClick={() => navigate(-1)}/>
                <p>Album Share/<span>{folder}</span></p>
            </div>
            {
                images.length != 0 ?
                    <div className={"photo-container"}>
                        {
                            images.map((image, index) => (

                                <div onClick={() => {
                                    handleShowSlide(index)
                                }} className="photo" key={index}>
                                    <img
                                        src={image.imageURL}
                                        alt={image.image_name}/>
                                </div>

                            ))
                        }
                    </div> :
                    <>
                        <div className="empty-img">
                            <img src={emptyImage} alt=""/>
                            <p>Ready to add some photo</p>
                        </div>
                    </>
            }
            <Modal
                title="Select an Image to store"
                centered
                open={newFolder}
                onOk={() => handleUploadImageToFolder()}
                onCancel={() => setNewFolder(false)}
            >
                <div className='choose-image-to-store'>
                    <div className='image'>
                        {imageAdd.map((image, index) => (
                            <div key={index}
                                 onClick={handleChooseImage(index, image._id)}
                            >
                                {
                                    exitImage.includes(index) ? (
                                        <div className='choose-image'>
                                            <img src={image.imageURL} alt=""/>
                                        </div>
                                    ) : (
                                        <div className=''>
                                            <img src={image.imageURL} alt=""/>
                                        </div>
                                    )
                                }
                            </div>
                        ))}
                    </div>
                </div>
            </Modal>
            <div className="profile-bar-upload" onClick={() => setNewFolder(true)}>
                <div className="upload">
                    <div className="upload-button">
                        <FontAwesomeIcon icon={faArrowUpFromBracket}/>
                    </div>
                </div>
            </div>

        </>
    )
}


export default ImageInShareFolder