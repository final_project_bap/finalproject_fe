import {album, images} from "../../models";
import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {RootState} from "../../app/store";

export interface AlbumState {
    loading: boolean
    albumShare: album[]
}

const initialState: AlbumState = {
    loading: false,
    albumShare: []
}

const shareAlbumSlice = createSlice({
    name: "shareAlbum",
    initialState,
    reducers: {

        gettingAlbumShare(state) {
            state.loading = true
        },
        getAlbumShareSuccess(state, action) {
            state.loading = false
            state.albumShare = action.payload
        },
        getAlbumShareFailed(state, action: PayloadAction<string>) {
            state.loading = false
        },
        shareFolder(state, action) {
            state.loading = true
        },
        shareFolderSuccess(state) {
            state.loading = false
        },
        shareFolderFailed(state, action) {
            state.loading = false
        }
    }


})

export const shareAlbumActions = shareAlbumSlice.actions

export const selectAlbumShare = (state: RootState) => state.shareAlbum.albumShare
export const selectAlbumShareLoading = (state: RootState) => state.shareAlbum.loading


const shareAlbumReducer = shareAlbumSlice.reducer
export default shareAlbumReducer