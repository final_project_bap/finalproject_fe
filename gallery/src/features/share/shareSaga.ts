import {all, call, put, takeEvery, takeLatest} from "redux-saga/effects";

import {album, listResponse} from "../../models";
import albumApi from "../../api/albumApi";
import {shareAlbumActions} from "./shareSlice";












function* handleGetAlbumShare(){
    try {

        const response: listResponse<album> = yield call(albumApi.getShareAlbum)
        yield put(shareAlbumActions.getAlbumShareSuccess(response.data))
    }
    catch (e){
        console.log(e)
    }
}
function* watchGetAlbumShare(){
    yield takeEvery(shareAlbumActions.gettingAlbumShare.type, handleGetAlbumShare)
}



export function* shareSaga(){
    yield all([
        watchGetAlbumShare()
    ])

}