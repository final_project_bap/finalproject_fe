import {album, images, shareAlbum} from "../../models";
import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {RootState} from "../../app/store";

export interface AlbumState {
    loading: boolean
    gallery: images[]
    album: album[]
}

const initialState : AlbumState = {
    loading: false,
    gallery: [],
    album:[]
}

const homeSlice = createSlice({
    name: "home",
    initialState,
    reducers :{
        gettingGallery(state, action : PayloadAction<string>){
            state.loading = true
        },
        getGallerySuccess(state, action :PayloadAction<images[]>){
            state.loading = false
            state.gallery = action.payload
        },
        getGalleryFailed(state, action){
            state.loading = false
        },

        gettingAlbum(state){
            state.loading = true
        },
        getAlbumSuccess(state, action){
            state.loading = false
            state.album = action.payload
        },
        getAlbumFailed(state, action: PayloadAction<string>){
            state.loading = false
        },

        createNewFolder(state, action){
            state.loading = true
        },
        createNewFolderSuccess(state){
            state.loading = false
        },
        createNewFolderFailed(state, action){
            state.loading = false
        },


        shareFolder(state, action){
            state.loading = true
        },
        shareFolderSuccess(state){
            state.loading = false
        },
        shareFolderFailed(state){
            state.loading = false
        }


    }


})

export const homeActions = homeSlice.actions

export const selectGallery = (state: RootState) => state.home.gallery
export const selectAlbum = (state: RootState) => state.home.album
export const selectLoading = (state: RootState) => state.home.loading


const homeReducer = homeSlice.reducer
export default homeReducer