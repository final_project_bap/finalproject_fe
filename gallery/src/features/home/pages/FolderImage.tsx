import React, {ReactNode, useEffect, useState} from "react";
import {useAppDispatch, useAppSelector} from "../../../app/hooks";
import {homeActions, selectAlbum, selectLoading} from "../homeSlice";
import {Modal, Select} from "antd";
import {faFolder, faPlus, faShare, faTrash} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {ContextMenu, ContextMenuTrigger, MenuItem} from "react-contextmenu";
import {Link} from "react-router-dom";
import LoaderBanner from "../../../components/loader/loader";
import albumApi from "../../../api/albumApi";
import {album} from "../../../models";
import  emptyFolder from  "./544262.png"

export const FolderImage = () => {
    const [nameFolder, setNameFolder] = useState("")
    const [newFolder, setNewFolder] = useState(false)
    const [email, setEmail] = useState<string[]>([]);
    const [id, setId] = useState("")
    const handleChange = (value: any) => {
        setEmail(value);
    };
    const loading = useAppSelector(selectLoading)
    const [share, setShare] = useState(false)
    const dispatch = useAppDispatch()
    useEffect(() => {
        dispatch(homeActions.gettingAlbum())
    }, [dispatch])
    const listAlbum = useAppSelector(selectAlbum)
    const handelCreateFolder = () => {
        dispatch(homeActions.createNewFolder(nameFolder))
        setNewFolder(false)
    }
    const handleDelete = async (idDelete: any) => {
        albumApi.deleteAlbum(idDelete)
            .then((res) => {
                dispatch(homeActions.gettingAlbum())
            })
            .catch((error) => {
                console.log(error);
            })
    }
    const handleOnclickShare = (id: string) => {
        setId(id)
        setShare(true)
    }
    const handleOnOkShareAlbum = () => {

        dispatch(homeActions.shareFolder({album_id: id, listEmail: email }))

        setShare(true)
    }

    return (

        <>
            {loading && <LoaderBanner/>}

            <div className="folder-container">
                <div className="row">
                    {listAlbum.length >1 ? listAlbum.map((item: album, index: number) => (
                        <>
                            <ContextMenuTrigger id={`image-context-menu-${index}`} key={index}>
                                <Link to={`/${item.album_name}/${item._id}`} key={index}>
                                    <div className="col-md-6 folder-container">
                                        <FontAwesomeIcon className={"icon-folder"} icon={faFolder}/>
                                        <span>{item.album_name}</span>
                                    </div>
                                </Link>

                            </ContextMenuTrigger>
                            <ContextMenu id={`image-context-menu-${index}`}>
                                <div className="action-with-image"

                                >
                                    <div onClick={() => handleDelete(item._id)} className="delete">
                                        <MenuItem className='svg'>
                                            <FontAwesomeIcon icon={faTrash}/>
                                            <p>Delete</p>
                                        </MenuItem>
                                    </div>
                                    <div onClick={() => {
                                        handleOnclickShare(item._id)
                                    }} className="share">
                                        <MenuItem className='svg'>
                                            <FontAwesomeIcon icon={faShare}/>
                                            <p>Share</p>
                                        </MenuItem>
                                    </div>
                                </div>
                            </ContextMenu>
                        </>
                    )):
                        <>
                            <div className="empty-folder">
                                <img src={emptyFolder} alt=""/>
                                <p>no folder</p>
                            </div>
                        </>

                    }
                </div>
            </div>
            <Modal
                title="Share folder with..."

                onOk={handleOnOkShareAlbum}
                open={share}
                onCancel={() => setShare(false)}
            >
                <Select
                    mode="tags"
                    style={{
                        width: '100%',
                    }}

                    onChange={handleChange}
                    tokenSeparators={[',']}
                />
            </Modal>
            <Modal
                title="New Folder"
                centered
                zIndex={100000}
                open={newFolder}
                onOk={() => handelCreateFolder()}
                onCancel={() => setNewFolder(false)}
            >
                <input type="text" placeholder='New folder' value={nameFolder}
                       onChange={(e) => setNameFolder(e.target.value)}
                       style={{width: "300px", height: "40px", borderRadius: "4px", paddingLeft: "5px"}}/>
            </Modal>

            <div className="profile-bar-upload">
                <div onClick={() => {
                    setNewFolder(true)
                }} className="upload">
                    <div className="upload-button"

                    >
                        <FontAwesomeIcon icon={faPlus}/>
                    </div>
                </div>
            </div>
        </>
    )
}
