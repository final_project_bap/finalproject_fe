import {faArrowLeft, faArrowUpFromBracket, faCopy, faShare, faTrash} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import React, {useEffect, useState} from 'react'
import {useNavigate, useParams} from 'react-router-dom'
import "./homePages.css"
import {Modal} from 'antd'
import {useAppDispatch, useAppSelector} from '../../../app/hooks'
import {ContextMenu, ContextMenuTrigger, MenuItem} from 'react-contextmenu'
import {homeActions, selectGallery} from "../homeSlice";
import imageApi from "../../../api/imageApi";
import {imageActions, selectListImageResponse} from "../../image/imageSlice";
import emptyImage from "../../image/pages/Gallery-PNG-Download-Image.png"

export const ImageOfFolder = () => {
    const {folder} = useParams()
    const {id}: any = useParams()
    const navigate = useNavigate()
    const [newFolder, setNewFolder] = useState(false)
    const dispatch = useAppDispatch()
    const [exitImage, setExitImage] = useState<number[]>([])
    const [idImage, setIdImage] = useState<any[]>([])

    useEffect(() => {
        dispatch(imageActions.gettingImages())
        dispatch(homeActions.gettingGallery(id))
    }, [dispatch])

    const images = useAppSelector(selectGallery)
    const imageAdd = useAppSelector(selectListImageResponse)
    const [border, setBorder] = useState<string[]>(
        Array(images.length).fill("")
    )


    const handleChooseImage = (index: number, id: string) => () => {


        const updateBorder = [...border]
        updateBorder[index] = border[index - 1] == "choose-image" ? "" : "choose-image"
        setBorder(updateBorder)
        if (exitImage.includes(index)) {
            setExitImage(exitImage.filter((indexImage) => indexImage != index))
        } else {
            setExitImage([...exitImage, index])
        }
        if (idImage.includes(id)) {
            setIdImage(idImage.filter((idImage) => idImage != id))
        } else {
            setIdImage([...idImage, id])
        }
    }

    const handleUploadImageToFolder = () => {

        imageApi.uploadImageToFolderApi(id, idImage)
            .then((res: any) => {
                dispatch(homeActions.gettingGallery(id))
                setNewFolder(false)
            })
            .catch((error) => {
                console.log(error);
            })
    }

    const handleDelete = (idDelete: string) => {
        imageApi.deleteImageOfAlbum(id, idDelete)
            .then((res) => {
                dispatch(homeActions.gettingGallery(id))
            })
            .catch((error) => {
                console.log(error);
            })
    }

    return (
        <>
            <div className='turn-back-page'>
                <FontAwesomeIcon icon={faArrowLeft} onClick={() => navigate(-1)}/>
                <p>Album/<span>{folder}</span></p>
            </div>

            <div className="photo-container">
                {images[0] != null ? images.map((item, index) => (

                    <>
                        <ContextMenuTrigger id={`image-context-menu-${index}`}>
                            <div className="photo" key={index}>
                                <img
                                    src={item.imageURL}
                                    alt={item.image_name}/>
                            </div>
                        </ContextMenuTrigger>
                        <ContextMenu id={`image-context-menu-${index}`}>
                            <div className="action-with-image"
                                 onClick={() => {
                                     handleDelete(item._id)
                                 }}
                            >
                                <div className="delete">
                                    <MenuItem className='svg'>
                                        <FontAwesomeIcon icon={faTrash}/> {" "} Delete
                                    </MenuItem>
                                </div>

                            </div>
                        </ContextMenu>                   
                    </>
                )): <>
                    <div className="empty-img">
                        <img src={emptyImage} alt=""/>
                        <p>Ready to add some photo</p>
                    </div>
                </>

                }
            </div>

            <Modal
                title="Select an Image to store"
                centered
                open={newFolder}
                onOk={() => handleUploadImageToFolder()}
                onCancel={() => setNewFolder(false)}
            >
                <div className='choose-image-to-store'>
                    <div className='image'>
                        {imageAdd.map((image, index) => (
                            <div key={index}
                                 onClick={handleChooseImage(index, image._id)}
                            >
                                {
                                    exitImage.includes(index) ? (
                                        <div className='choose-image'>
                                            <img src={image.imageURL} alt=""/>
                                        </div>
                                    ) : (
                                        <div className=''>
                                            <img src={image.imageURL} alt=""/>
                                        </div>
                                    )
                                }
                            </div>
                        ))}
                    </div>
                </div>
            </Modal>
            <div className="profile-bar-upload" onClick={() => setNewFolder(true)}>
                <div className="upload">
                    <div className="upload-button">
                        <FontAwesomeIcon icon={faArrowUpFromBracket}/>
                    </div>
                </div>
            </div>
        </>
    )
}
