import {all, call, put, takeEvery, takeLatest} from "redux-saga/effects";
import {homeActions} from "./homeSlice";
import {album, images, listResponse, shareAlbum} from "../../models";
import albumApi from "../../api/albumApi";
import {PayloadAction} from "@reduxjs/toolkit";
import {imageActions} from "../image/imageSlice";
import {toast} from "react-toastify";



function* handleGetAlbum(){
    try {
        yield put(homeActions.gettingAlbum)
        const response: listResponse<album> = yield call(albumApi.getAlbumOfUser)
        yield put(homeActions.getAlbumSuccess(response.data))
    }
    catch (e){
        console.log(e)
    }
}

function* handleGetGallery( action: PayloadAction<string>){
    try {
        const response: listResponse<images> = yield call(albumApi.getImageOfAlbum, action.payload)
        yield put(homeActions.getGallerySuccess(response.data))

    }
    catch (e){
        console.log(e)
    }
}



function* watchGetGallery() {
    yield takeEvery(homeActions.gettingGallery.type, handleGetGallery);
}

function* watchGetAlbum(){
    yield takeLatest(homeActions.gettingAlbum.type, handleGetAlbum)
}



function* watchCreateNewFolder() {
    yield takeEvery(homeActions.createNewFolder.type, handleCreateNewFolder)
}
function* handleCreateNewFolder(action: PayloadAction<string>) {
    try {
        const data:string = yield call(albumApi.createAlbum, action.payload);
        yield put(homeActions.gettingAlbum())
        toast.success(`create success folder ${action.payload}`)
        yield put(homeActions.createNewFolderSuccess())

    } catch (error) {
        toast.warning("create failed failed")

        yield put(imageActions.uploadImageFailed())
    }
}

export interface dataShareImage{
    code: number,
    message: string
}
function* watchShareFolder() {
    yield takeEvery(homeActions.shareFolder.type, handleShareFolder)
}
function* handleShareFolder(action: PayloadAction<shareAlbum>) {
    try {
        const data:dataShareImage[] = yield call(albumApi.shareAlbum, action.payload);
        yield put(homeActions.gettingAlbum())
        yield put(homeActions.shareFolderSuccess())

        Array.from(data).forEach((item, index) => {
            if(item.code == 404){
                toast.warning(item.message)
            }else {
                toast.success(item.message)
            }
        });

    } catch (error) {
        yield put(homeActions.shareFolderFailed())
    }
}


export function* homeSaga(){
    yield all([
        watchGetAlbum(),
        watchShareFolder(),
        watchGetGallery(),
        watchCreateNewFolder()
    ])

}



