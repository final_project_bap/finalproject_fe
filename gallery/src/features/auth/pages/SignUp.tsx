import React, { useState } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import {useAppDispatch, useAppSelector} from "../../../app/hooks";
import {authActions, selectLoading} from "../authSlice";
import {ClipLoader} from "react-spinners";

export const SignUp =() => {
    const [register, setRegister] = useState({ name: "", email: "", password: "" })
    const navigate = useNavigate()
    const dispatch = useAppDispatch()
    let [color, setColor] = useState("green");
    const loading = useAppSelector(selectLoading)
    const handleRegister = async (e: any) => {
        e.preventDefault()
        dispatch(authActions.register(register))
        navigate("/login")
    }
    return (
        <>
            <div className='body'>
                <div className="wrapper">
                    <form action="" onSubmit={handleRegister} method='POST'>
                        <h1>Register</h1>
                        <div className="input-box">
                            <input type="text" name='name' placeholder="Full name" required onChange={(e) => setRegister((preState) => ({ ...preState, name: e.target.value }))} />
                            <i className='bx bxs-user'></i>
                        </div>
                        <div className="input-box">
                            <input type="email" name='email' placeholder="Email" required onChange={(e) => setRegister((preState) => ({ ...preState, email: e.target.value }))} />
                            <i className='bx bxs-lock-alt' ></i>
                        </div>
                        <div className="input-box">
                            <input type="password" name='password' placeholder="Password" required onChange={(e) => setRegister((preState) => ({ ...preState, password: e.target.value }))} />
                            <i className='bx bxs-lock-alt' ></i>
                        </div>
                        <button type="submit" className="btn-login"><p>Register</p>
                            <ClipLoader
                                color={color}
                                loading={loading}
                                size={20}
                                aria-label="Loading Spinner"
                                data-testid="loader"/></button>
                        <div className="register-link">
                            <p>Already have a count? <Link to="/login">Login</Link></p>
                        </div>
                    </form>
                </div>
            </div>
        </>
    )
}
