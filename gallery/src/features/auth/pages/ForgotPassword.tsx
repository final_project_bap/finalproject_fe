import React, {useState} from 'react'
import {Link} from 'react-router-dom'
import "./forgotPassword.css"
import {useAppDispatch, useAppSelector} from "../../../app/hooks";
import {authActions, selectLoading} from "../authSlice";
import {ClipLoader} from "react-spinners";

export const ForgotPassword = () => {
    const [email, setEmail] = useState("")
    const dispatch= useAppDispatch()
    let [color, setColor] = useState("green");
    const loading = useAppSelector(selectLoading)
    const handleForgotPassword = async (e: React.ChangeEvent<HTMLFormElement>) => {
        e.preventDefault()

        dispatch(authActions.forgotPassword(email))

    }
    return (
        <>
            <div className="container-">
                <div className="body-form">
                    <form action="" method="post" onSubmit={handleForgotPassword}>
                        <div className="title-form">
                            <p>Enter the email address associated with your account
                                and we'll send you a link to reset your password.</p>
                        </div>
                        <div className="input-form">
                            <input type="email" name="" id="" placeholder='Email'
                                   onChange={(e) => setEmail(e.target.value)}/>
                        </div>
                        <div className="btn-login btn-form">
                            <button>Continue</button>
                            <ClipLoader
                                color={color}
                                loading={loading}
                                size={20}
                                aria-label="Loading Spinner"
                                data-testid="loader"/>
                        </div>
                        <div className="link-signup">
                            <p>Don't have an account? <Link className='link' to={"/auth/register"}>Register</Link></p>
                        </div>
                    </form>
                </div>
            </div>
        </>
    )
}
