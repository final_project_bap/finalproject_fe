import React, {useState} from 'react'
import {Link, useParams} from 'react-router-dom'
import authApi from "../../../api/authApi";
import {useAppSelector} from "../../../app/hooks";
import {selectLoading} from "../authSlice";
import {ClipLoader} from "react-spinners";
import  {useNavigate} from "react-router-dom";
import  {toast} from "react-toastify";

export const RecoverPassword = () => {

    const token = useParams()
    const loading = useAppSelector(selectLoading)
    const navigate=useNavigate()
    const [password, setPassword] = useState("")
    let [color, setColor] = useState("green");

    const handleResetPassword = async (e: React.ChangeEvent<HTMLFormElement>) => {
        e.preventDefault()
        try {
            await authApi.resetPassword(token,password)
            navigate("/")
            toast.success("Reset new password  successfully")
        }catch (e){
            console.log(e)
        }
    }
    return (
        <>
            <div className="container-">
                <div className="body-form">
                    <form action="" method="post" onSubmit={handleResetPassword}>
                        <div className="title-form">
                            <p>Enter your new password</p>
                        </div>
                        <div className="input-form">
                            <input type="password" name="" id="" placeholder='New password'
                                   onChange={(e) => setPassword(e.target.value)}/>
                        </div>
                        <div className="btn-form" >
                            <button>Reset Password</button>
                        </div>
                        <div className="link-signup">
                            <p>Already have a count? <Link className='link' to="/">Login</Link></p>
                        </div>
                    </form>
                </div>
            </div>
        </>
    )
}
