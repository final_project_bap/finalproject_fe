import {authActions} from "./authSlice";
import {loginModel} from "../../models/loginModel";
import {PayloadAction} from "@reduxjs/toolkit";
import {put, take, all, fork, call, takeLatest} from 'redux-saga/effects';
import {toast} from 'react-toastify';
import {commonResponse, CreateUser, tokenResponse, User} from "../../models";
import {deleteAccessToken, getAccessToken} from "../../utils";
import authApi from "../../api/authApi";
function* handleLogin(payload: loginModel) {
    try {
        const data: tokenResponse = yield call(authApi.login, payload);
        if (data) {
            yield put(authActions.loginSuccess());
        }
        yield put(authActions.getCurrentUser());
    } catch (error: any) {
        yield put(authActions.loginFailed(error.message));
        toast.warning("email or password invalid")

    }
}

function* watchLoginFlow() {
    while (true) {
        const isLoggedIn = Boolean(getAccessToken());
        if (!isLoggedIn) {
            const action: PayloadAction<loginModel> = yield take(authActions.login.type);
            yield fork(handleLogin, action.payload)
        } else {
            const action: PayloadAction<string> = yield take(authActions.logout.type);
            yield fork(handleLogout,action.payload)
        }
    }

}

function* handleLogout( payload : string) {
    try {
        deleteAccessToken()
        yield call(authApi.logout, payload)
        yield put(authActions.logout)
    }catch (e){
        console.log(e)
    }
}


function* handleForgotPassword(payload: string) {
    try {
        const data: commonResponse<string>  = yield call(authApi.forgotPassword, payload);
        console.log(data)
        if(data.code == 200){
            put(authActions.forgotPasswordSuccess())
            toast.success(data.message)
        }
        yield put(authActions.forgotPasswordFailed());

    }catch (e:any){

        yield put(authActions.forgotPasswordFailed());
        if (e.code == 400){
            toast.warning(e.message)
        }
        else {
            toast.warning(e.message)

        }
    }
}

function* watchForgotPassword() {
    while (true) {
        const action: PayloadAction<string> = yield take(authActions.forgotPassword.type)
        yield fork(handleForgotPassword, action.payload)
    }
}






function* handleRegister(payload: CreateUser) {

    try {
        const data: commonResponse<User> = yield call(authApi.register, payload);
        if (data.code == 201) {
            yield put(authActions.registerSuccess());
            toast.success(data.message)

        }

    } catch (e:any) {
        yield put(authActions.registerFailed());
        if (e.code == 400){
            toast.warning(e.message)
        }
        else {
            toast.warning(e.message)

        }
    }
}

function* watchRegisterFlow() {
    while (true) {
        const action: PayloadAction<CreateUser> = yield take(authActions.register.type)
        yield fork(handleRegister, action.payload)
    }
}

export function* authSaga() {

    yield all([
        call(watchLoginFlow),
        call(watchRegisterFlow),
        call(watchForgotPassword)
        // call(watchGetCurrentUser),
    ]);
}
