
import {all, call, put, take, takeEvery, takeLatest} from 'redux-saga/effects';

import {images, listResponse, uploadImage} from "../../models";

import {imageActions} from "./imageSlice";
import imageApi from "../../api/imageApi";
import {PayloadAction} from "@reduxjs/toolkit";
import {toast} from "react-toastify";
import albumApi from "../../api/albumApi";
import {homeActions} from "../home/homeSlice";






function* fetchImages() {
    try {

        yield put(imageActions.gettingImages);
        const response: listResponse<images> = yield call(imageApi.getImages);
        yield put(imageActions.getImagesSuccess(response.data) );
    } catch (error) {
        console.error('Error fetching images:', error);
    }
}


function* fetchFavouriteImages() {
    try {
        yield put(imageActions.gettingFavouriteImages);
        const response:listResponse<images> = yield call(imageApi.getFavouriteImages);
        yield put(imageActions.getFavouriteImagesSuccess(response.data));

    } catch (error) {
        console.error('Error fetching favorite images:', error);
    }
}


function* watchGetImages() {
    yield takeLatest(imageActions.gettingImages.type, fetchImages);
}


function* watchUploadImages() {
    yield takeEvery(imageActions.uploadImage.type, handleUploadImage);
}


function* handleUploadImage(action: PayloadAction<FileList>) {
    try {
        const data:string = yield call(imageApi.uploadImage, action.payload);
        yield put(imageActions.gettingImages())
        yield put(imageActions.uploadImageSuccess())
        toast.success("add success")

    } catch (error) {
        toast.warning("add failed")

        yield put(imageActions.uploadImageFailed())
    }
}


function* watchGetFavouriteImages() {
    yield takeLatest(imageActions.gettingFavouriteImages, fetchFavouriteImages);
}


export function* imageSaga() {
    yield all([
        watchGetImages(),
        watchGetFavouriteImages(),
        watchUploadImages()
    ]);
}


