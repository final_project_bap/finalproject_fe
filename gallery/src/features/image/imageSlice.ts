import {images, } from "../../models";
import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {RootState} from "../../app/store";


export interface ImageState {
    loading: boolean
    listImageResponse: images[]
}

const initialState: ImageState = {
    loading: false,
    listImageResponse: []
}

const imageSlice = createSlice({
    name:"image",
    initialState,
    reducers:{
        gettingImageInAlbum(state, action : PayloadAction<string>){
            state.loading = true
        },
        getImageInAlbumSuccess(state, action :PayloadAction<images[]>){
            state.loading = false
            state.listImageResponse = action.payload
        },
        getImageInAlbumFailed(state, action){
            state.loading = false
        },

        gettingImages(state) {
            state.loading = true
        },
        gettingFavouriteImages(state) {
            state.loading = true
        },
        getFavouriteImagesSuccess(state, action: PayloadAction<images[]>) {
            state.loading = false
            state.listImageResponse = action.payload
        },
        getImagesSuccess(state, action: PayloadAction<images[]>) {
            state.loading = false
            state.listImageResponse = action.payload
        },
        getImagesFailed(state) {
            state.loading = false
        },
        uploadImage(state, action:PayloadAction<FileList>){
            state.loading  = true
        } ,
        uploadImageSuccess(state){
            state.loading  = false
        },
        uploadImageFailed(state){
            state.loading  = false
        },

        addFavouriteImage(state, action :PayloadAction<string>){
            state.loading = false
        }


    }
})
export const imageActions = imageSlice.actions

export const selectListImageResponse = (state:RootState) => state.image.listImageResponse
export const selectLoading = (state:RootState) => state.image.loading

const imageReducer = imageSlice.reducer;

export default imageReducer