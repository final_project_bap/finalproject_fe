import "./showimages.css"
import {useAppDispatch, useAppSelector} from "../../../app/hooks";
import {imageActions, selectListImageResponse} from "../imageSlice";
import {useEffect} from "react";
import {selectLoading} from "../imageSlice";
import LoaderBanner from "../../../components/loader/loader";
import {selectImageActions} from "../../../components/SelectImage/selectImageSlice";
import emptyImage from "./Gallery-PNG-Download-Image.png"

const showImages = () => {
    const dispatch = useAppDispatch()
    useEffect(() => {
        dispatch(imageActions.gettingImages())
    }, [dispatch])
    const loading = useAppSelector(selectLoading)
    const images = useAppSelector(selectListImageResponse) || []
    const handleShowSlide = (index: number) => {
        dispatch(selectImageActions.selectingImage({index: index, images: images}))
    }

    return (
        <>
            {loading && <LoaderBanner/>}
            {

                images.length != 0 ?
                    <div className={"photo-container"}>
                        {
                            images.map((image, index) => (

                                <div onClick={() => {
                                    handleShowSlide(index)
                                }} className="photo" key={index}>
                                    <img
                                        src={image.imageURL}
                                        alt={image.image_name}/>
                                </div>

                            ))
                        }
                    </div> :
                    <>
                        <div className="empty-img">
                            <img src={emptyImage} alt=""/>
                            <p>Ready to add some photo</p>
                        </div>
                    </>
            }


        </>
    )
}

export default showImages