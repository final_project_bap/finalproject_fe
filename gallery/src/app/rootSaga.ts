import {authSaga} from "../features/auth/authSaga";
import {all} from 'redux-saga/effects';
import {imageSaga} from "../features/image/imageSaga";
import {homeSaga} from "../features/home/homeSaga";
import {selectImageSaga} from "../components/SelectImage/selectImageSaga";
import {shareSaga} from "../features/share/shareSaga";

export default function* rootSaga() {
    yield all([authSaga(), imageSaga(), homeSaga(), selectImageSaga(),shareSaga()]);
}