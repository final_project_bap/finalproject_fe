import { configureStore, ThunkAction, Action } from "@reduxjs/toolkit"
import createSagaMiddleWare from 'redux-saga'
import authReducer from "../features/auth/authSlice";
import rootSaga from "./rootSaga";
import imageReducer from "../features/image/imageSlice";
import homeReducer from "../features/home/homeSlice";
import selectImageReducer from "../components/SelectImage/selectImageSlice";
import loadingReducer from "../components/loader/loadingSlice";
import shareAlbumReducer from "../features/share/shareSlice";

const sagaMiddleWare = createSagaMiddleWare()
export const store = configureStore({
  reducer: {
    auth: authReducer,
    image: imageReducer,
    home: homeReducer,
    selectImage: selectImageReducer,
    loading: loadingReducer,
    shareAlbum: shareAlbumReducer,
  },
  middleware: (getDefaultMiddleware) => getDefaultMiddleware({ serializableCheck: false }).concat(sagaMiddleWare)
})

sagaMiddleWare.run(rootSaga)
export type AppDispatch = typeof store.dispatch
export type RootState = ReturnType<typeof store.getState>
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>
