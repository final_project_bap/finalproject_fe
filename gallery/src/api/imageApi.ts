import {images, listResponse, uploadImage} from "../models";
import axiosClient from "./axiosClient";

const imageApi ={
    getImages: (): Promise<listResponse<images>> =>{
        const url = "/images"
        return axiosClient.get(url)
    },
    getFavouriteImages: () : Promise<listResponse<images>> => {
        const url = "images/favorite"
        return axiosClient.get(url)
    },

    removeFavouriteImage: (imageId: string) =>{
      const url = "/images/remove/favorite"
      return axiosClient.post(url, {imageId: imageId})
    },
    addFavouriteImage: (imageId: string) =>{
      const url = "/images/favorite/"
      return axiosClient.post(url, {imageId: imageId})
    },
    uploadImage:  (data: FileList) : Promise<string> => {
        const formData = new FormData();
        Array.from(data).forEach((file, index) => {
            formData.append(`image`, file);
        });
        const url = "/images/upload/"
        return   axiosClient.post(url, formData)
    },
    download: (id: string) =>{
        const url = "/images/download/"
        return axiosClient.post(url, {imageId: id})
     },
    deleteImage: (public_id:any) => {
        const url = `images/remove/${public_id}`
        return axiosClient.delete(url)
    },
    deleteImageOfAlbum: (id:any,removeImages:string) => {
        const url = `album/removeImages/${id}`
        console.log("payload", removeImages)
        return axiosClient.put(url,{removeImages:removeImages})
    },
    uploadImageToFolderApi:  (id:any,image:any) => {
        const url = `album/addImageIntoAlbum/${id}`

        return axiosClient.post(url,{
            images:image
        })
    },
    shareImage: (imageId: string) => {
        const url = "/images/share"
        return axiosClient.post(url, {imageId: imageId })
    },
    getImageShare: () => {
        const url = 'getShareImages'
    }



}

export default imageApi