import axiosClient from "./axiosClient";

const userApi = {
    currentUser: () =>{
        const url =  "current_user"
        return axiosClient.get(url)
    },

}