import axiosClient from "./axiosClient";
import {CreateUser, listResponse} from "../models";
import {loginModel} from "../models/loginModel";
import {Params} from "react-router-dom";

const authApi = {
    login: (data: loginModel)  => {
        const url = '/auth/login';
        return axiosClient.post(url, data);
    },
    register: (data : CreateUser) : Promise<CreateUser> =>{
        const url = "/auth/signup"
        return axiosClient.post(url, data)
    },
    logout: (refreshToken:string) => {
        const url = "/auth/logout"
        return axiosClient.post(url,{refreshToken: refreshToken})
    },
    refreshToken: () => {
        const url = "/auth/refreshToken"
        return axiosClient.post(url)
    },
    forgotPassword: async (email: string)=>{
        const url = "user/forgotPassword"
        return axiosClient.post(url, {email:email})

    },
    resetPassword:  async (token: Readonly<Params<string>>, password: string) => {
        const url = `/user/resetPassword/${token.token}`
        return axiosClient.post(url, {newPassword:password})
    }
}

export default authApi;
