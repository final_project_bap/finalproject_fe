import axiosClient from "./axiosClient"
import {shareAlbum} from "../models";

const albumApi = {
    getAlbumOfUser: () => {
        const url = "/album/getAlbumsByUserId"
        return axiosClient.get(url)
    },
    getImageOfAlbum: (id: string) => {
        const url = `/album/getImageFromAlbum/${id}`
        return axiosClient.get(url)
    },
    createAlbum: (album_name: string) => {
        const url = "/album/createAlbum"
        return axiosClient.post(url, {album_name: album_name})
    },
    deleteAlbum: (albumId: any) => {
        const url = `album/deleteAlbum/${albumId}`
        return axiosClient.delete(url)
    },
    shareAlbum : (data: shareAlbum) =>{
        const url = `album/shareAlbum/${data.album_id}`
        return axiosClient.post(url,{recipients: data.listEmail })
    },
    getShareAlbum: () =>{
        const url = "album/getShareAlbums"
        return axiosClient.get(url)
    }

}

export default albumApi