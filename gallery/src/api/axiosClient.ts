import axios, {AxiosError, AxiosRequestConfig, AxiosResponse} from 'axios';
import queryString from "query-string";
import {getAccessToken} from "../utils";
import authApi from "./authApi"



const axiosClient = axios.create({
    baseURL: import.meta.env.BASE_URL,
    paramsSerializer: params => queryString.stringify(params),
    withCredentials: true
});
axiosClient.interceptors.request.use(
    (config) => {
        if (getAccessToken()) {
            config.headers["Authorization"] = `Bearer ${getAccessToken()}`
        }
        return config
    },
    (error) => {
        return Promise.reject(error)
    }
)

interface CustomAxiosRequestConfig extends AxiosRequestConfig {
    isRetry?: boolean;
}

axiosClient.interceptors.response.use(
    async (response: AxiosResponse) => {
        const config: CustomAxiosRequestConfig = response.config;
        if (config.url?.includes("/login") || config.url?.includes("user/forgotPassword")) {
            return response.data;
        }

        if (response.data.code === 401 && !config.isRetry) {
            try {
                const newToken = await authApi.refreshToken();
                console.log("Token mới:", newToken);

                if (config.headers) {
                    config.headers["Authorization"] = `Bearer ${newToken}`;
                }
                console.log("Làm mới token thành công:", newToken);
                config.isRetry = true;
                return axiosClient(config);
            } catch (refreshError) {
                console.error("Lỗi khi làm mới token:", refreshError);
                return Promise.reject(response);
            }
        }

        return response.data;
    },
    (error: AxiosError) => {
        return Promise.reject(error.response ? error.response.data : error.message);
    }
);


export default axiosClient;
