import { defineConfig } from "vitest/config"
import react from "@vitejs/plugin-react"
import env from 'dotenv'
env.config()
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  server: {
    open: true,
  },
  build: {
    outDir: "build",
    sourcemap: true,
  },
  test: {
    globals: true,
    environment: "jsdom",
    setupFiles: "src/setupTests",
    mockReset: true,
  },
  define: {
    "import.meta.env.BASE_URL": `"${process.env.BASE_URL}"`
  }
})
